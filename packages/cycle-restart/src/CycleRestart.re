open CycleRun;

[@bs.deriving abstract]
type restartableOpts = {
  [@bs.optional]
  pauseSinksWhileReplaying: bool,
};

[@bs.module "cycle-restart"]
external rerunner:
  (cycle_setup, unit => 'sources) => (. main('sources, 'sinks)) => unit =
  "";
[@bs.module "cycle-restart"]
external restartable: ('driver, ~opts: restartableOpts=?, unit) => 'driver =
  "";