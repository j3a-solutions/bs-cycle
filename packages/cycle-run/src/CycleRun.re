/* TODO: fantasy land observable dictionaries */
type main('sources, 'sinks) = 'sources => 'sinks;

type disposeFunction = unit => unit;

[@bs.module "@cycle/run"]
external run: (main('sources, 'sinks), 'sources) => disposeFunction = "";

/* TODO: type input and output */
type cycle_setup;
[@bs.module "@cycle/run"] [@bs.val] external setup: cycle_setup = "";