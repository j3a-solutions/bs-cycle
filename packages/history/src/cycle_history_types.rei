module PushHistoryInput: {type t('a);};

let make_push_history_input:
  (~state: 'a=?, string) => PushHistoryInput.t(option('a));

module ReplaceHistoryInput: {type t('a);};

let make_replace_history_input:
  (~state: 'a=?, string) => ReplaceHistoryInput.t(option('a));

type go_history_input;
let make_go_history_input: int => go_history_input;

type go_back_history_input;
let make_go_back_history_input: unit => go_back_history_input;

type go_forward_history_input;
let make_go_forward_history_input: unit => go_forward_history_input;