type pathname = string;

type history_input_type =
  | Push
  | Replace;

let str_of_input_type = x =>
  switch (x) {
  | Push => "push"
  | Replace => "replace"
  };

module type InputConfig = {let typeName: history_input_type;};

module MakeHistoryInput = (T: InputConfig) => {
  let kind = T.typeName;
  [@bs.deriving abstract]
  type t('a) = {
    [@bs.as "type"]
    type_: string,
    [@bs.optional]
    pathname,
    [@bs.optional]
    state: 'a,
  };
  let make = (~state=?, pathname: string) =>
    switch (state) {
    | Some(x) => t(~type_=str_of_input_type(kind), ~pathname, ~state=x, ())
    | None => t(~type_=str_of_input_type(kind), ~pathname, ())
    };
};

module PushHistoryInput =
  MakeHistoryInput({
    let typeName = Push;
  });

let make_push_history_input = (~state=?, pathname) =>
  PushHistoryInput.make(~state, pathname);

module ReplaceHistoryInput =
  MakeHistoryInput({
    let typeName = Replace;
  });

let make_replace_history_input = (~state=?, pathname) =>
  ReplaceHistoryInput.make(~state, pathname);

[@bs.deriving abstract]
type go_history_input = {
  [@bs.as "type"]
  type_: string,
  amount: int,
};

let make_go_history_input = amount => go_history_input(~type_="go", ~amount);

[@bs.deriving abstract]
type go_back_history_input = {
  [@bs.as "type"]
  type_: string,
};

let make_go_back_history_input = () => go_back_history_input(~type_="goBack");

[@bs.deriving abstract]
type go_forward_history_input = {
  [@bs.as "type"]
  type_: string,
};

let make_go_forward_history_input = () =>
  go_forward_history_input(~type_="goForward");