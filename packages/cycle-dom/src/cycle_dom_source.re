open Cycle_dom_stream;

[@bs.deriving abstract]
type eventsFnOptions = {
  [@bs.optional]
  useCapture: bool,
  [@bs.optional]
  passive: bool,
  [@bs.optional]
  bubbles: bool,
  [@bs.optional]
  preventDefault: Cycle_dom_from_event.preventDefaultOpt,
};

type domSourceElement = [
  | `Document(Dom.document)
    /* TODO: is there a HTMLBodyElement in webapi bindings? */
  | `HTMLBodyElement(Dom.element)
  | `ArrayElement(Dom.element)
  | `Str(string)
];

type domSource;
type domSink = Node_stream.t;

[@bs.send] external select: (domSource, string) => domSource = "";
[@bs.send] external elements: domSource => Elements_stream.t = "";

[@bs.send] external events: (domSource, string) => Events_stream.t = "events";

[@bs.send]
external eventsWithOpts:
  (
    domSource,
    [@bs.string] [ | `click],
    eventsFnOptions,
    ~bubbles: bool=?,
    unit
  ) =>
  Events_stream.t =
  "events";