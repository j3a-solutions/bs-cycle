open Snabbdom;

/* Type cast an element's textContent method to avoid polymorphic variants.
   Use snabbdom hyperscript function instead if you prefer stricter type checks. Probably better */
external textNodes: string => array(vnode) = "%identity";
external textNode: string => vnode = "%identity";

/* Empty is an alias for empty elements. Usage is equivalent of unit in labelled arguments */
let empty: array(vnode) = [||];

/* Snabbdom hyperscript function is variadic instead of allowing optional args.
   This helper injects defaults for named tags */
let helper =
    (
      ~className: string="",
      ~tag: string,
      /* CSS selector for id and class names */
      ~sel: string="",
      ~data: vData=vData(),
      children: array(vnode),
    ) =>
  Snabbdom.h(tag ++ sel ++ className, data, `Children(children));

/* Series of functions. Allows for better tree-shaking */

/* SVG */
/* let a = helper(~tag="a"); */
let altGlyph = helper(~tag="altGlyph");
let altGlyphDef = helper(~tag="altGlyphDef");
let altGlyphItem = helper(~tag="altGlyphItem");
let animate = helper(~tag="animate");
let animateColor = helper(~tag="animateColor");
let animateMotion = helper(~tag="animateMotion");
let animateTransform = helper(~tag="animateTransform");
let circle = helper(~tag="circle");
let clipPath = helper(~tag="clipPath");
let colorProfile = helper(~tag="colorProfile");
let cursor = helper(~tag="cursor");
let defs = helper(~tag="defs");
let desc = helper(~tag="desc");
let ellipse = helper(~tag="ellipse");
let feBlend = helper(~tag="feBlend");
let feColorMatrix = helper(~tag="feColorMatrix");
let feComponentTransfer = helper(~tag="feComponentTransfer");
let feComposite = helper(~tag="feComposite");
let feConvolveMatrix = helper(~tag="feConvolveMatrix");
let feDiffuseLighting = helper(~tag="feDiffuseLighting");
let feDisplacementMap = helper(~tag="feDisplacementMap");
let feDistantLight = helper(~tag="feDistantLight");
let feFlood = helper(~tag="feFlood");
let feFuncA = helper(~tag="feFuncA");
let feFuncB = helper(~tag="feFuncB");
let feFuncG = helper(~tag="feFuncG");
let feFuncR = helper(~tag="feFuncR");
let feGaussianBlur = helper(~tag="feGaussianBlur");
let feImage = helper(~tag="feImage");
let feMerge = helper(~tag="feMerge");
let feMergeNode = helper(~tag="feMergeNode");
let feMorphology = helper(~tag="feMorphology");
let feOffset = helper(~tag="feOffset");
let fePointLight = helper(~tag="fePointLight");
let feSpecularLighting = helper(~tag="feSpecularLighting");
let feSpotlight = helper(~tag="feSpotlight");
let feTile = helper(~tag="feTile");
let feTurbulence = helper(~tag="feTurbulence");
let filter = helper(~tag="filter");
let font = helper(~tag="font");
let fontFace = helper(~tag="fontFace");
let fontFaceFormat = helper(~tag="fontFaceFormat");
let fontFaceName = helper(~tag="fontFaceName");
let fontFaceSrc = helper(~tag="fontFaceSrc");
let fontFaceUri = helper(~tag="fontFaceUri");
let foreignObject = helper(~tag="foreignObject");
let g = helper(~tag="g");
let glyph = helper(~tag="glyph");
let glyphRef = helper(~tag="glyphRef");
let hkern = helper(~tag="hkern");
let image = helper(~tag="image");
let line = helper(~tag="line");
let linearGradient = helper(~tag="linearGradient");
let marker = helper(~tag="marker");
let mask = helper(~tag="mask");
let metadata = helper(~tag="metadata");
let missingGlyph = helper(~tag="missingGlyph");
let mpath = helper(~tag="mpath");
let path = helper(~tag="path");
let pattern = helper(~tag="pattern");
let polygon = helper(~tag="polygon");
let polyline = helper(~tag="polyline");
let radialGradient = helper(~tag="radialGradient");
let rect = helper(~tag="rect");
/* let script = helper(~tag="script"); */
let set = helper(~tag="set");
let stop = helper(~tag="stop");
/* let style = helper(~tag="style"); */
let switch_ = helper(~tag="switch");
let symbol = helper(~tag="symbol");
let text = helper(~tag="text");
let textPath = helper(~tag="textPath");
/* let title = helper(~tag="title"); */
let tref = helper(~tag="tref");
let tspan = helper(~tag="tspan");
let use = helper(~tag="use");
let view = helper(~tag="view");
let vkern = helper(~tag="vkern");

/* TAGS */
let a = helper(~tag="a");
let abbr = helper(~tag="abbr");
let address = helper(~tag="address");
let area = helper(~tag="area");
let article = helper(~tag="article");
let aside = helper(~tag="aside");
let audio = helper(~tag="audio");
let b = helper(~tag="b");
let base = helper(~tag="base");
let bdi = helper(~tag="bdi");
let bdo = helper(~tag="bdo");
let blockquote = helper(~tag="blockquote");
let body = helper(~tag="body");
let br = helper(~tag="br");
let button = helper(~tag="button");
let canvas = helper(~tag="canvas");
let caption = helper(~tag="caption");
let cite = helper(~tag="cite");
let code = helper(~tag="code");
let col = helper(~tag="col");
let colgroup = helper(~tag="colgroup");
let dd = helper(~tag="dd");
let del = helper(~tag="del");
let details = helper(~tag="details");
let dfn = helper(~tag="dfn");
let dir = helper(~tag="dir");
let div = helper(~tag="div");
let dl = helper(~tag="dl");
let dt = helper(~tag="dt");
let em = helper(~tag="em");
let embed = helper(~tag="embed");
let fieldset = helper(~tag="fieldset");
let figcaption = helper(~tag="figcaption");
let figure = helper(~tag="figure");
let footer = helper(~tag="footer");
let form = helper(~tag="form");
let h1 = helper(~tag="h1");
let h2 = helper(~tag="h2");
let h3 = helper(~tag="h3");
let h4 = helper(~tag="h4");
let h5 = helper(~tag="h5");
let h6 = helper(~tag="h6");
let head = helper(~tag="head");
let header = helper(~tag="header");
let hgroup = helper(~tag="hgroup");
let hr = helper(~tag="hr");
let html = helper(~tag="html");
let i = helper(~tag="i");
let iframe = helper(~tag="iframe");
let img = helper(~tag="img");
let input = helper(~tag="input");
let ins = helper(~tag="ins");
let kbd = helper(~tag="kbd");
let keygen = helper(~tag="keygen");
let label = helper(~tag="label");
let legend = helper(~tag="legend");
let li = helper(~tag="li");
let link = helper(~tag="link");
let main = helper(~tag="main");
/* avoid shadowing of map */
let mapHtml = helper(~tag="map");
let mark = helper(~tag="mark");
let menu = helper(~tag="menu");
let meta = helper(~tag="meta");
let nav = helper(~tag="nav");
let noscript = helper(~tag="noscript");
let object_ = helper(~tag="object");
let ol = helper(~tag="ol");
let optgroup = helper(~tag="optgroup");
let option = helper(~tag="option");
let p = helper(~tag="p");
let param = helper(~tag="param");
let pre = helper(~tag="pre");
let progress = helper(~tag="progress");
let q = helper(~tag="q");
let rp = helper(~tag="rp");
let rt = helper(~tag="rt");
let ruby = helper(~tag="ruby");
let s = helper(~tag="s");
let samp = helper(~tag="samp");
let script = helper(~tag="script");
let section = helper(~tag="section");
let select_ = helper(~tag="select");
let small = helper(~tag="small");
let source = helper(~tag="source");
let span = helper(~tag="span");
let strong = helper(~tag="strong");
let style = helper(~tag="style");
let sub = helper(~tag="sub");
let summary = helper(~tag="summary");
let sup = helper(~tag="sup");
let table = helper(~tag="table");
let tbody = helper(~tag="tbody");
let td = helper(~tag="td");
let textarea = helper(~tag="textarea");
let tfoot = helper(~tag="tfoot");
let th = helper(~tag="th");
let thead = helper(~tag="thead");
let time = helper(~tag="time");
let title = helper(~tag="title");
let tr = helper(~tag="tr");
let u = helper(~tag="u");
let ul = helper(~tag="ul");
let video = helper(~tag="video");