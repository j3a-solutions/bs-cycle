let textContent = x => [|ReasonReact.string(x)|];

/* TODO: import directly from file when module is exposed in @cycle/react */

[@bs.module "@cycle/react"]
external h:
  (string, Js.t({..}), array(ReasonReact.reactElement)) =>
  ReasonReact.reactElement =
  "h";

[@bs.module "@cycle/react"]
external hWithClass:
  (
    [@bs.unwrap] [
      | `Stateless(ReasonReact.stateless)
      | `Class(ReasonReact.reactClass)
    ],
    ~props: Js.t({..})=?,
    array(ReasonReact.reactElement)
  ) =>
  ReasonReact.reactElement =
  "h";