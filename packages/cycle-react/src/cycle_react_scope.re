open Cycle_react_stream;

type handler = Scope_stream.t;
type handlers = Js.Dict.t(Js.Dict.t(handlerType));

type listener = unit => unit;
type listeners = Js.Dict.t(listener);

type unsubscriber = unit => unit;

type scope = {
  .
  "handlers": handlers,
  "listeners": listeners,
  [@bs.meth] "getSelectorHandlers": string => Js.Dict.t(handler),
  [@bs.meth] "getHandler": (string, string) => handler,
  [@bs.meth] "subscribe": (string, listener) => unsubscriber,
};

[@bs.new] external createScope: unit => scope = "Scope";