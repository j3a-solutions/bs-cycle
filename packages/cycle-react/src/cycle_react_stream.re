open Xstream;

/* TODO: scope.t */
type handlerType;

module Scope_stream =
  Memory.Make_memory_stream({
    type ok = handlerType;
    type error = Js.Exn.t;
  });

module Element_stream =
  Memory.Make_memory_stream({
    type ok = ReasonReact.reactElement;
    type error = Js.Exn.t;
  });

module Event_stream =
  Memory.Make_memory_stream({
    type ok = ReactEvent.Synthetic.t;
    type error = Js.Exn.t;
  });