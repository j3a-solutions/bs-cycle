open Cycle_react_scope;
open Cycle_react_stream;
open Xstream;

type sink = Element_stream.t;

module ReactSource = {
  type props;

  module Stream =
    Memory.Make_memory_stream({
      type ok = props;
      type error = Js.Exn.t;
    });

  [@bs.deriving abstract]
  type reactSource('props) = {
    _selector: option(string),
    _scope: scope,
    [@bs.as "_props$"]
    _props: Stream.t,
    _childScopes: Js.Dict.t(scope),
  };

  type t = reactSource(props);

  [@bs.new]
  external jsMake:
    (
      ~scope: scope,
      ~selector: string=?,
      /* TODO: createWithMemory default */
      ~props: Stream.t
    ) =>
    reactSource(props) =
    "ReactSource";
};

module MakeReactSource = (Type: {type props;}) => {
  type props = Type.props;
  module Stream =
    Memory.Make_memory_stream({
      type ok = props;
      type error = Js.Exn.t;
    });

  [@bs.deriving abstract]
  type reactSource('props) = {
    _selector: option(string),
    _scope: scope,
    [@bs.as "_props$"]
    _props: Stream.t,
    _childScopes: Js.Dict.t(scope),
  };

  type t = reactSource(props);
  [@bs.new]
  external jsMake:
    (~scope: scope, ~selector: string=?, ~props: Stream.t) =>
    reactSource(props) =
    "ReactSource";
};

[@bs.send]
external select: (ReactSource.t, Js_types.symbol) => ReactSource.t = "";
[@bs.send]
external selectStr: (ReactSource.t, string) => ReactSource.t = "select";
[@bs.send] external events: (ReactSource.t, string) => Event_stream.t = "";
[@bs.send] external props: (ReactSource.t, unit) => ReactSource.Stream.t = "";
[@bs.send] external isolateSink: (ReactSource.t, string) => scope = "";
[@bs.send] external getChildScope: (ReactSource.t, sink, string) => sink = "";
[@bs.send]
external isolateSource: (ReactSource.t, ReactSource.t, string) => ReactSource.t =
  "";