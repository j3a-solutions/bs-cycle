open CycleDom;
open CycleRun;
open Cycle_dom_h;

type sources = {. "dom": domSource};
type sinks = {. "dom": domSink};

let main: main(sources, sinks) =
  sources => {
    open Cycle_dom_stream.Node_stream;
    let action =
      merge([|
        sources##dom->select(".decrement")->events("click")->map(_ => (-1)),
        sources##dom->select(".increment")->events("click")->map(_ => 1),
      |]);

    let count = action->fold((acc, x) => acc + x, 0);
    let vdom =
      count->mapFromMemory(count =>
        div([|
          button(~sel=".decrement", textNodes("Decrement")),
          button(~sel=".increment", textNodes("Increment")),
          p(textNodes("Counter: " ++ string_of_int(count))),
        |])
      );
    {"dom": vdom};
  };

run(main, {"dom": makeDOMDriverWithSelector("#app", domDriverOptions())});